<!-- Login form start -->
<form id="userLoginForm" onsubmit="loginUser(event)">
	@csrf
	<div class="form-element square login">
		<label>Username
			<span>**</span>
		</label>
		<input type="text" name="username" placeholder="Username ...."
			   class="input-field-square"
			   value="{{ old('username') }}">
		<p class="eml error"></p>
	</div>

	<div class="form-element square login">
		<label>Password
			<span>**</span>
		</label>
		<input type="password" name="password" placeholder="Password"
			   class="input-field-square">
		<p class="eml error"></p>
	</div>
	<button type="submit" class="submit-form-btn">login</button>
</form>
<br>
<a href="{{ route('password.request') }}">Forget Password ??</a>
<!-- Login form End -->




<!-- Register Form Start -->
<form id="userRegisterForm" onsubmit="registerUser(event)">
	@csrf
	<div class="form-element square login">
		<label> Full Name <span>**</span></label>
		<input type="text" name="name" placeholder="Type your fullname....  " class="input-field-square">
		<p class="emr error"></p>
	</div>
	<div class="form-element square login">
		<label>Username <span>**</span> </label>
		<input type="text" name="username" placeholder="Type username...." class="input-field-square">
		<p class="emr error"></p>
	</div>
	<div class="form-element square login">
		<label> Email <span>**</span> </label>
		<input type="email" name="email" placeholder="Type your email...."
			   class="input-field-square">
		<p class="emr error"></p>
	</div>
	<div class="form-element square login">
		<label>Phone Number <span>**</span> </label>
		<input type="text" name="phone" placeholder="Type your phone number...."
			   class="input-field-square">
		<p class="emr error"></p>
	</div>
	<div class="form-element square login">
		<label>Password <span>**</span></label>
		<input type="password" name="password" placeholder="Type new password...."
			   class="input-field-square">
		<p class="emr error"></p>
	</div>
	<div class="form-element square login">
		<label>Confirm Password <span>**</span></label>
		<input type="password" name="password_confirmation" placeholder="Type confirm password...."
			   class="input-field-square">
	</div>
	<input type="submit" class="submit-form-btn" value="Sign Up">
</form>
<!-- Register Form End -->







<script>
     function loginUser(event) {
        event.preventDefault();

        var form = document.getElementById('form-login');
        var fd = new FormData(form);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: "{{route('modal.login')}}",
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,

            success: function(data){
                console.log(data)

                var  em = document.getElementsByClassName('error');

                for (let i = 0 ; i<em.length; i++) {
                    em[i].innerHTML = '';
                }

                if(data === "success") {
                    window.location = '{{route('home')}}';
                }
                if (data == "no_success") {
                    $('.error_login_username').text('Username/Password do not match!')
                    $('.login-pass-field').val('');
                }
                    if(typeof data.error != 'undefined') {

                        if(typeof data.username != 'undefined') {
                            $('.error_login_username').text(data.username);
                            $('.login-pass-field').val('');
                        }
                        if(typeof data.password != 'undefined') {
                            $('.error_login_password').text(data.password)
                        }
                    }
            }
        })
    }


    function registerUser(e) {
        e.preventDefault();
        var form = document.getElementById('userRegisterForm');
        var fd = new FormData(form);
        $.ajax({
            url: '{{route('modal.register')}}',
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log(data);
                var em = document.getElementsByClassName("emr");
                // after returning from the controller we are clearing the
                // previous error messages...
                for(i=0; i<em.length; i++) {
                    em[i].innerHTML = '';
                }
                if(data === "success") {
                    window.location = '{{route('home')}}';
                }
                // Showing error messages in the HTML...
                if(typeof data.error != 'undefined') {
                    if(typeof data.name != 'undefined') {
                        em[0].innerHTML = data.name[0];
                    }
                    if(typeof data.username != 'undefined') {
                        em[1].innerHTML = data.username[0];
                    }
                    if(typeof data.email != 'undefined') {
                        em[2].innerHTML = data.email[0];
                    }
                    if(typeof data.phone != 'undefined') {
                        em[3].innerHTML = data.phone[0];
                    }
                    if(typeof data.password != 'undefined') {
                        em[4].innerHTML = data.password[0];
                    }
                }
            }
        });
    }
</script>
