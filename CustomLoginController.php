<?php

namespace App\Http\Controllers;

use App\GeneralSettings;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\UserLogin;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;


class CustomLoginController extends Controller
{
    protected $redirectTo = '/user/home';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function username()
    {
        return 'username';
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string',
            'password' => 'required'
        ]);
        if($validator->fails()) {
            // adding an extra field 'error'...
            $validator->errors()->add('error', 'true');
            return response()->json($validator->errors());
        }

        if (Auth::attempt([
            'username' => $request->username,
            'password' => $request->password,
        ])) {
            return "success";
        } else {
            return "no_success";
        }

    }



}
